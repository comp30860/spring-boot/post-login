package ie.ucd.hellospringboot;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class AuthenticationController {
    @PostMapping("/login")
    public String login(Credentials credentials, Model model) {
        if (credentials.getUsername().equals("rem") &&
            credentials.getPassword().equals("sausage")) {
            return "success";
        }
        return "failure";
    }
}